package com.devcamp.task53b_60;

public class Mammal extends Animal {

    public Mammal(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return String.format("Mammal[Animal [Name = %s]]", name);
    }

}
