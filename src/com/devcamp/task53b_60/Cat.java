package com.devcamp.task53b_60;

public class Cat extends Mammal {

    public Cat(String name) {
        super(name);
    }

    public void greets() {
        System.out.println("Meow");
    }

    @Override
    public String toString() {
        return String.format("Cat[Mammal[Animal [Name = %s]]]", name);
    }

}
